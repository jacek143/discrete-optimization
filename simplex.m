function [x,fval,exitflag] = simplex(f,A,b,tolerance)
% x in R, min(f), Ax<=b
%
%exitflag:
%   1   Function converged to a solution x.
%   -2  No feasible point was found.
%   -3  Problem is unbounded.

f = -f; % changing from min to max

new_f = [f,f];
for i = 1:length(f)
    new_f(2*i-1) = f(i);
    new_f(2*i) = -f(i);
end
f = new_f;

new_A = [A,A];
for i = 1:size(A,2)
    new_A(:,2*i-1) = A(:,i);
    new_A(:,2*i) = -A(:,i);
end
A = new_A;

x = zeros(length(f)/2,1);
fval = 0;

[tableau, base, exitflag] = simplex1(f,A,b,tolerance);

if exitflag~=1
    return
end

[tableau,base,exitflag] = simplex2(tableau,base,tolerance);

x_tmp = zeros(length(f),1);
if exitflag == 1
    for i = 1:length(base)
        x_tmp(base(i)) = tableau(i,end);
    end
    x_tmp = x_tmp(1:length(f));
    for i = 1:length(x)
       x(i) = x_tmp(2*i-1)-x_tmp(2*i);
    end
    fval = tableau(end,end);
end

function [tableau,base,exitflag] = simplex1(f,A,b,tolerance)
if ~sum(b<0)
    tableau = [A,eye(size(A,1)),b;f,zeros(1,size(A,1)),0];
    base = (1:size(A,1))+size(A,2);
    exitflag = 1;
    return;
end

base = (1:size(A,1))+size(A,2);
tableau = [A,eye(size(A,1)),b;f,zeros(1,size(A,1)),0];
tableau = [tableau;zeros(1,size(tableau,2))];
s = [];
for i = 1:size(tableau,1)
    if tableau(i,end) < 0
        s = [s,i];
        tableau(i,:) = tableau(i,:)*(-1);
        tableau = [tableau(:,1:end-1),zeros(size(tableau,1),1),tableau(:,end)];
        tableau(i,end-1) = 1;
        tableau(end,end-1) = -1;
        base(i) = length(tableau)-1;
    end
end
for i = 1:length(s)
   tableau(end,:) = tableau(end,:)+tableau(s(i),:); 
end

[tableau,base,exitflag] = simplex2(tableau,base,tolerance);

if exitflag ~= 1
   return; 
end

for i = 1:length(base)
    if base(i) > (size(A,2)+size(A,1)) && base(i) < size(tableau,2)
        exitflag = -2; %infeasible
        return;
    end
end

%if tableau(end,end) ~= 
if tableau(end,end) > tolerance && tableau(end,end) < -tolerance;
   exitflag = -3; %unbounded
   return;
end

tableau = [tableau(:,1:(size(A,2)+size(A,1))),tableau(:,end)];
tableau = tableau(1:(end-1),:);


function [tableau,base,exitflag] = simplex2(tableau,base,tolerance)
while ~isoptimal(tableau,tolerance)
    [c] = find(tableau(end,1:(end-1))>0,1);
    if sum(tableau(1:(end-1),c)<=0)==(size(tableau,1)-1)
        exitflag = -3; %unbounded
        return;
    end
    r = [];
    min_piv = Inf;
    for i = 1:length(base)
        if tableau(i,end)/tableau(i,c) < min_piv ...
                && tableau(i,c) > 0
            r = i;
            min_piv = tableau(r,end)/tableau(r,c);
        end
    end
    tableau(r,:) = tableau(r,:)/tableau(r,c);
    base(r) = c;
    for i = [1:(r-1),(r+1):size(tableau,1)]
        tableau(i,:) = tableau(i,:) - tableau(r,:)*tableau(i,c);
    end
end
exitflag = 1;

function [result] = isoptimal(tableau,tolerance)
result = sum(tableau(end,1:(end-1))<=tolerance)==(size(tableau,2)-1);