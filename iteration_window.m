function varargout = iteration_window(varargin)
% ITERATION_WINDOW MATLAB code for iteration_window.fig
%      ITERATION_WINDOW, by itself, creates a new ITERATION_WINDOW or raises the existing
%      singleton*.
%
%      H = ITERATION_WINDOW returns the handle to a new ITERATION_WINDOW or the handle to
%      the existing singleton*.
%
%      ITERATION_WINDOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ITERATION_WINDOW.M with the given input arguments.
%
%      ITERATION_WINDOW('Property','Value',...) creates a new ITERATION_WINDOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before iteration_window_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to iteration_window_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help iteration_window

% Last Modified by GUIDE v2.5 27-Apr-2016 00:21:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @iteration_window_OpeningFcn, ...
    'gui_OutputFcn',  @iteration_window_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before iteration_window is made visible.
function iteration_window_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to iteration_window (see VARARGIN)

% Choose default command line output for iteration_window
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes iteration_window wait for user response (see UIRESUME)
% uiwait(handles.figure1);
Solution = getappdata(0,'Solution');
iterations = Solution.iterations;

str = sprintf('%10s', 'No)');
str =strcat(str,sprintf('%10s', 'exit flag'));
str =strcat(str,sprintf('%10s', 'fval'));
for i = 1:(size(iterations,1)-2)
    s = sprintf('x_%d',i);
    str = strcat(str,sprintf('%10s',s));
end
set(handles.text_header,'String',str);

str = '[';
for i = 1:size(iterations,2)
    if i~=1
        str = strcat(str,';');
    end
    it = iterations(:,i);
    str = strcat(str,'''');
    s = sprintf('%d)',i);
    str = strcat(str,sprintf('%10s',s));
    for j = 1:numel(it)
        s = sprintf('%.2f',it(j));
        str = strcat(str,sprintf('%10s',s));
    end
    str = strcat(str,'''');
end
str = strcat(str,']');
set(handles.text,'String',eval(str));

% --- Outputs from this function are returned to the command line.
function varargout = iteration_window_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
