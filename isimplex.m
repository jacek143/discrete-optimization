function [x,fval,exitflag,iterations] = isimplex(f,A,b,tolerance)
% x in Z, min(f), Ax<=b, tolerance >= 0
%
%exitflag:
%   1   Function converged to a solution x.
%   -2  No feasible point was found.
%   -3  Problem is unbounded.

[x,fval,exitflag] = simplex(f,A,b,tolerance);
iterations = [exitflag;fval;x];

if exitflag == -2 || exitflag == -3
    return;
end

if sum(abs(x-round(x))<=tolerance) == length(x)
    exitflag = 1;
    return;
end

[m,i] = min(abs((x - floor(x))-0.5));

b1 = [b;floor(x(i))];
A1 = [A;zeros(1,size(A,2))];
A1(end,i) = 1;
[x1_rel,fval1_rel,exitflag1_rel] = simplex(f,A1,b1,tolerance);

b2 = [b;-ceil(x(i))];
A2 = [A;zeros(1,size(A,2))];
A2(end,i) = -1;
[x2_rel,fval2_rel,exitflag2_rel] = simplex(f,A2,b2,tolerance);

if exitflag1_rel~=1 && exitflag2_rel==1
    [x,fval,exitflag,it] = isimplex(f,A2,b2,tolerance);
    iterations = [iterations,it];
    return;
elseif exitflag1_rel==1 && exitflag2_rel~=1
    [x,fval,exitflag,it] = isimplex(f,A1,b1,tolerance);
    iterations = [iterations,it];
    return;
elseif exitflag1_rel~=1 && exitflag2_rel~=1
    exitflag = -2;
    return;
end

if fval1_rel > fval2_rel
    [A1,A2] = deal(A2,A1);
    [b1,b2] = deal(b2,b1);
    [x1_rel,x2_rel] = deal(x2_rel,x1_rel);
    [fval1_rel,fval2_rel] = deal(fval2_rel,fval1_rel);
    [exitflag1_rel,exitflag2_rel] = deal(exitflag2_rel,exitflag1_rel);
end

%now fval1_rel <= fval2_rel
[x1,fval1,exitflag1,it1] = isimplex(f,A1,b1,tolerance);
iterations = [iterations,it1];
if exitflag1~=1
    [x,fval,exitflag,it2] = isimplex(f,A2,b2,tolerance);
    iterations = [iterations,it2];
    return;
end
if fval1 <= fval2_rel
    x = x1;
    fval = fval1;
    exitflag = exitflag1;
    return
end
[x2,fval2,exitflag2,it2] = isimplex(f,A2,b2,tolerance);
iterations = [iterations,it2];
if exitflag2~=1
    x = x1;
    fval = fval1;
    exitflag = exitflag1;
    return;
end
if fval1 <= fval2
    x = x1;
    fval = fval1;
    exitflag = exitflag1;
    return;
else
    x = x2;
    fval = fval2;
    exitflag = exitflag2;
    return;
end