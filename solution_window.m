function varargout = solution_window(varargin)
% SOLUTION_WINDOW MATLAB code for solution_window.fig
%      SOLUTION_WINDOW, by itself, creates a new SOLUTION_WINDOW or raises the existing
%      singleton*.
%
%      H = SOLUTION_WINDOW returns the handle to a new SOLUTION_WINDOW or the handle to
%      the existing singleton*.
%
%      SOLUTION_WINDOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SOLUTION_WINDOW.M with the given input arguments.
%
%      SOLUTION_WINDOW('Property','Value',...) creates a new SOLUTION_WINDOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before solution_window_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to solution_window_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help solution_window

% Last Modified by GUIDE v2.5 26-Apr-2016 23:41:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @solution_window_OpeningFcn, ...
    'gui_OutputFcn',  @solution_window_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before solution_window is made visible.
function solution_window_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to solution_window (see VARARGIN)

% Choose default command line output for solution_window
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes solution_window wait for user response (see UIRESUME)
% uiwait(handles.figure1);
Solution = getappdata(0,'Solution');
exitflag = Solution.exitflag;
if exitflag == 1
    str = sprintf('Function converged to the solution x.\n');
elseif exitflag == -2
    str = sprintf('No feasible point was found.\n');
else % exitflag == -3
    str = sprintf('Problem is unbounded.\n');
end
set(handles.text,'String',str);

if exitflag == 1
    
    fval = Solution.fval;
    str = sprintf('f(x)=%f',fval);
    set(handles.text_f,'String',str);
    
    x = Solution.x;
    str = sprintf('x=[');
    for i = 1:numel(x)
        s = sprintf('%d',x(i));
        str = strcat(str,s);
        if i~=numel(x)
            str = strcat(str,'; ');
        end
    end
    s = sprintf(']');
    str = strcat(str,s);
    set(handles.text_x,'String',str);
else
    set(handles.text_x,'String',sprintf(''));
    set(handles.text_f,'String',sprintf(''));
end

% --- Outputs from this function are returned to the command line.
function varargout = solution_window_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_iterations.
function pushbutton_iterations_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_iterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
iteration_window;