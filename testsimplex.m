function [ result ] = testsimplex(f,A,b)
%result:
%   1   Good
%   0   Bad

opt = optimset('Display','none');
tolerance = 10^(-8);

[x1,fval1,exitflag1] = simplex(f,A,b,tolerance);
[x2,fval2,exitflag2] = linprog(f,A,b,[],[],[],[],[],opt);

if exitflag1 ~= exitflag2
    result = 0;
    return;
elseif exitflag1 ~= 1
    result = 1;
    return;
end

if sum(abs(x1-x2)>tolerance)
    result = 0;
    return;
end

if sum(abs(fval1-fval2)>tolerance)
    result = 0;
    return;
end

result = 1;
