\documentclass[10pt,a4paper]{report}
\usepackage[ascii]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\author{Jacek Jankowski}
\title{Integer Linear Programming \\ Project Report}
\begin{document}

\maketitle

\section*{Project description}

The aim of the project was to create an application for solving linear integer problem: \\
$\max f(x) = c^T x$ \\
subject to: \\
$A_1 x \leq b_1$ \\
$A_2 x \geq b_2$ \\
$x \in \textbf{Z}^{n \times 1},
b_1 \in \textbf{R}^{m_1 \times 1},
b_2 \in \textbf{R}^{m_2 \times 1},
A_1 \in \textbf{R}^{m_1 \times n_1},
A_2 \in \textbf{R}^{m_2 \times n_2},
n = n_1+n_2 
$ \\

The program is based on II-phase simplex algorithm and Branch \& Bound strategy.
It implies some limitations on input data.
First of all, optimized functions and constraints must be linear.
Secondly, constraints must create bounded and convex set of feasible solutions.
Under those limitations, it is guaranteed that the algorithm will converged to the optimal solution \cite{coursera}.

\subsection*{B\&B strategy}
B\&B relaxes the original problem by changing solution domain from integer to real numbers.
This particular implementation use recursion to perform branching. The algorithm consists from the following steps:
\begin{enumerate}
\item Compute solution $x_{rel}^{*}$ for the relaxed problem.
\item If the relaxed problem is unbounded or unfeasible, terminate procedure with \textit{unbounded} or \textit{unfeasible} flag respectively. 
\item Find the decision variable with the biggest fractional part $x_{rel,n}^{*}$.
\item Create two branches by adding the constraint $x_n \leq \lfloor x_{rel,n}^{*} \rfloor$ to the first branch and $x_n \geq \lceil x_{rel,n}^{*} \rceil$ to the second branch.
\item Compute relaxation for both branches.
\item If both branches are unbounded or unfeasible, terminate procedure with \textit{unbounded} or \textit{unfeasible} flag respectively.
\item If only one branch is bounded and feasible, return the value of recursive call for this branch.
\item Else perform recursive call for branch with the best relaxation result.
\item If the received function value is better than the second branch relaxation, then return result of the first branch.
\item If the received function value is worse than the second branch relaxation, perform recursive call for the second branch. Then return result of the better branch.
\end{enumerate}

\subsection*{II-phase simplex procedure}
To build the simplex tableau, first, we need to change inequality constraints to equality constraints by adding slack variables \cite{fisher}.
When we have constraints in the form $Ax=b, x>0$ and objective function $f(x)=c^Tx$, we can construct simplex tableau:
\begin{center}
\begin{tabular}{|c|c|}
  \hline
  & \\
  $ \quad A \quad $ & $b$ \\
  & \\
  \hline
  $c^T$ & $0$ \\
  \hline
\end{tabular} 
\end{center}
In the separate vector B, there is stored information about witch variables belong to the Basic Feasible Solution (BFS).
The used algorithm is divided on 2 distinct phases. In the first phase we are looking for the first BFS by transforming our original problem to the other problem with obvious BFS and performing casual simplex procedure. The received result is one of BFS of our original problem. Then we can go to the second phase, i.e. perform simplex procedure once again.

\subsection*{Application description}
The whole program was written in the Matlab environment, both the algorithm and the user graphical interface. The GUI was created using a \textit{GUIDE} tool.
Examples of working application are in the figures \ref{main_window} and \ref{result_windows}.
In the main window we can put in data like in the figure \ref{main_window}.
The result of the algorithm is shown in 2 figures.
In the first figure, there are the optimal solution, the optimal value of the objective function and a short text message, if the algorithm converged to the optimal solution.
In the second figure, there is a list of all iterations.
\begin{figure}
\includegraphics[width=\textwidth]{img/main_window}
\caption{Main application window.}
\label{main_window}
\end{figure}
\begin{figure}
\includegraphics[width=\textwidth]{img/result_windows}
\caption{Result application windows.}
\label{result_windows}
\end{figure}

\section*{Example Problems}
The meaning of \textit{exit flag} in the iteration list is the following:
\begin{itemize}
\item $1$ - problem solved,
\item $-2$ - problem unfeasible,
\item $-3$ - problem unbounded.
\end{itemize}

\subsection*{Example 1 - unbounded problem}
$\max f(x) = 2x_1+x_2$ subject to \\
$
\begin{cases}
-x_1 + x_2 \leq 0 \\
x_2 \geq 0 \\
\end{cases} \\
$
This problem is unbounded. 
The algorithm stopped after 1 iteration: \\
\begin{tabular}{|c|c|c|c|c|}
	\hline
	No) & exit flag & $f(x^*)$ & $x_1$ & $x_2$ \\
	\hline
	$1)$ & $-3$ & $0$ & $0$ & $0$ \\
	\hline
\end{tabular}

\subsection*{Example 2 - unfeasible problem}
$\max f(x) = 2x_1+x_2$ subject to \\
$
\begin{cases}
-x_1 + x_2 \leq 0 \\
x_2 \geq 0 \\
x_1 \leq -5.5 \\
\end{cases} \\
$
This problem is unfeasible. 
The algorithm stopped after 1 iteration: \\
\begin{tabular}{|c|c|c|c|c|}
	\hline
	No) & exit flag & $f(x^*)$ & $x_1$ & $x_2$ \\
	\hline
	$1)$ & $-2$ & $0$ & $0$ & $0$ \\
	\hline
\end{tabular}

\subsection*{Example 3 - feasible problem}
$\max f(x) = 2x_1+x_2$ subject to \\
$
\begin{cases}
-x_1 + x_2 \leq 0 \\
x_2 \geq 0 \\
x_1 \leq 5.5 \\
\end{cases} \\
$
This problem converged to the optimal solution $f(x^*) = f([5,5]^T)=-15$ after 2 iterations: \\
\begin{tabular}{|c|c|c|c|c|}
	\hline
	No) & exit flag & $f(x^*)$ & $x_1$ & $x_2$ \\
	\hline
	$1)$ & $1$ & $-16.5$ & $5.5$ & $5.5$ \\
	\hline
	$2)$ & $1$ & $-15$ & $5$ & $5$ \\
	\hline
\end{tabular}

\subsection*{Example 4 - feasible problem}
$\max f(x) = x_1+x_2$ subject to \\
$
\begin{cases}
x_1 + x_2 \leq 4.5 \\
-x_1+x_2 \leq 5.5 \\
x_2 \geq 0 \\
\end{cases} \\
$
This problem converged to the optimal solution $f(x^*) = f([0,4]^T)=-4.5$ after 11 iterations: \\
\begin{tabular}{|c|c|c|c|c|}
	\hline
	No) & exit flag & $f(x^*)$ & $x_1$ & $x_2$ \\ \hline
	$1)$ & $1$ & $-4.5$ & $4.5$ & $0$ \\ \hline
	$2)$ & $1$ & $-4.5$ & $4$ & $0.5$ \\ \hline
	$3)$ & $1$ & $-4.5$ & $3.5$ & $1$ \\ \hline
	$4)$ & $1$ & $-4.5$ & $3$ & $1.5$ \\ \hline
	$5)$ & $1$ & $-4.5$ & $2.5$ & $2$ \\ \hline
	$6)$ & $1$ & $-4.5$ & $2$ & $2.5$ \\ \hline
	$7)$ & $1$ & $-4.5$ & $1.5$ & $3$ \\ \hline
	$8)$ & $1$ & $-4.5$ & $1$ & $3.5$ \\ \hline
	$9)$ & $1$ & $-4.5$ & $0.5$ & $4$ \\ \hline
	$10)$ & $1$ & $-4.5$ & $0$ & $4.5$ \\ \hline
	$11)$ & $1$ & $-4.5$ & $0$ & $4$ \\ \hline
\end{tabular}

\subsection*{Example 5 - real life problem\cite{coursera}}
This is a classical instance of one-dimensional knapsack problem that has real-life applications such as finding the most optimal way to cut materials.
Assume that we have a wooden beam with the length of 10 units and it can bu used to produce 3 different items.
Items successively have the length of 5, 8, 3 units and can be sold for 45, 48 and 35\$. Thus, the problem can be formulated in the following way: \\
$\max f(x) = 45x_1+48x_2+35x_3$ subject to \\
$
\begin{cases}
5x_1 + 8x_2 + 3x_3 \leq 10 \\
x_i \in \{0,1\} \\
\end{cases} \\
$
Every Binary constraint can be replaced by two inequality constraints, so finally we have: \\
$\max f(x) = 45x_1+48x_2+35x_3$ subject to \\
$
\begin{cases}
5x_1 + 8x_2 + 3x_3 \leq 10 \\
x_1 \leq 1 \\
x_1 \geq 0 \\
x_2 \leq 1 \\
x_2 \geq 0 \\
x_3 \leq 1 \\
x_3 \geq 0 \\
\end{cases} \\
$
This problem converged to the optimal solution $f(x^*) = f([1,0,1]^T)=80$ after 2 iterations: \\
\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	No) & exit flag & $f(x^*)$ & $x_1$ & $x_2$ & $x_3$ \\ \hline
	$1)$ & $1$ & $-92$ & $1$ & $0.25$ & $1$ \\ \hline
	$2)$ & $1$ & $-80$ & $1$ & $0$ & $1$ \\ \hline
\end{tabular}

\section*{Discussion of errors}
The program generally uses real numbers but in some critical places it must perform equality comparison of float variable and integer value.
To do this, the program check the following inequality: \\
$|x_{real}-x_{integer}| \leq \varepsilon$, \\
where $\varepsilon$ is the precision of computations.
The user can change this parameter in the main window in the box named \emph{Tolerance}.

\begin{thebibliography}{9}
  
\bibitem{fisher}
	Felix Fischer,
	\emph{Optimization - Lecture notes},
	University of Cambridge, Mathematical Tripos,
	date accessed: \date{May 8, 2016},
	\url{http://page.math.tu-berlin.de/~fischerf/teaching/opt/lectures.html}
  
\bibitem{larsen}
	Jesper Larsen,
	\emph{Solving Real-Life Problems with Integer Programming},
	Department of Management Engineering,
	Technical University of Denmark,
	date accessed: \date{May 8, 2016},
	\url{http://www2.imm.dtu.dk/courses/02713/motivation42113.pdf}
	
\bibitem{coursera}
	Pascal Van Hentenryck,
	\emph{Discrete Optimization},
	The University of Melbourne,
	date accessed: \date{May 8, 2016},
	\url{https://www.coursera.org/course/optimization}

\end{thebibliography}

\end{document}
