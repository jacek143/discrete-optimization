function [ result ] = testisimplex(f,A,b)
%result:
%   1   Good
%   0   Bad

opt = optimoptions('intlinprog','Display','none');
tolerance = 10^(-10);

[x1,fval1,exitflag1] = isimplex(f,A,b,tolerance);
[x2,fval2,exitflag2] = intlinprog(f,1:length(f),A,b,[],[],[],[],opt);

if exitflag1 ~= exitflag2
    result = 0;
    return;
elseif exitflag1 ~= 1
    result = 1;
    return;
end

if sum(abs(x1-x2)>tolerance)
    result = 0;
    return;
end

if sum(abs(fval1-fval2)>tolerance)
    result = 0;
    return;
end

result = 1;
